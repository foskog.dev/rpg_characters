﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    public class Mage : Character
    {
        /// <summary>
        /// Constructor for a mage class
        /// starts with:
        /// 5 Vitality
        /// 1 Strength
        /// 1 Dexterity
        /// 8 Intelligence
        /// </summary>
        /// <param name="name"></param>
        public Mage(string name) : base(name)
        {
            Name = name;
            PrimaryAttributes = new PrimaryAttributes(5, 1, 1, 8);
            SecondaryAttributes = new SecondaryAttributes();
            SecondaryAttributes = SecondaryAttributes.CalculateSecondaryAttributes(PrimaryAttributes);
            CharacterDPS = CalculateCharacterDPS();
        }
        /// <summary>
        /// Weapon equip method for mage, equips to weapon slot in equipment dictionary. Can equip Staves and wands. Throws InvalidWeaponException If weapon is of an incompatible WeaponType or Character level is too low. 
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>Success Message as a string.</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public override string Equip(Weapon weapon)
        {
            if (weapon.WeaponType != WeaponTypes.Staff &&
                weapon.WeaponType != WeaponTypes.Wand)
            {
                throw new InvalidWeaponException($"This class cannot use a {weapon.WeaponType}");
            }
            if (Level < weapon.Level)
            {
                throw new InvalidWeaponException($"{Name} is not a high enough level to equip {weapon.Name},  level {weapon.Level} is required");
            }
            // This block replaces the weapon and recalculates the characters DPS based on the new weapon.
            Equipment[weapon.EquipmentSlot] = weapon;
            CalculateCharacterDPS();
            return $"{weapon.Name} equipped";
        }
        /// <summary>
        /// armor equip method for mage, equips to respective armor slot on item in equipment dictionary. Can equip Cloth  armor. Throws InvalidArmorException if armor is of an incompatible ArmorType or Character level is too low.
        /// If the armor slot is currently occupied. the attributes from the occuping item will be subtracted from characters attributes before the new ones are added.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>Success Message as a string.</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public override string Equip(Armor armor)
        {
            if (armor.ArmorType != ArmorTypes.Cloth)
            {
                throw new InvalidArmorException($"This class cannot wear {armor.ArmorType}");
            }
            else if (Level < armor.Level)
            {
                throw new InvalidArmorException($"{Name} is not a high enough level to equip {armor.Name},  level {armor.Level} is required");
            }
            // This block subtracts the currently equipped armors attributes before changing the equipped armor.
            // Then it recalculates the secondaryAttributes based on the new total primaryAttributes.
            var currentArmor = (Armor)Equipment[armor.EquipmentSlot];
            PrimaryAttributes -= currentArmor.PrimaryAttributes;
            Equipment[armor.EquipmentSlot] = armor;
            PrimaryAttributes += armor.PrimaryAttributes;
            SecondaryAttributes = SecondaryAttributes.CalculateSecondaryAttributes(PrimaryAttributes);
            CalculateCharacterDPS();
            return $"{armor.Name} equipped in {armor.EquipmentSlot} slot";
        }
        /// <summary>
        /// Increases character level by one. Creates a new set of primaryAttributes according appropriate for level up on the current class. Adds them to the total PrimaryAttributes and recalculates SecondaryAttributes.
        /// </summary>
        public override void LevelUp()
        {
            Level += 1;
            Console.WriteLine($"{Name} leveled up to level {Level}");
            PrimaryAttributes gainedAttributes = new PrimaryAttributes(3, 1, 1, 5);
            PrimaryAttributes += gainedAttributes;
            SecondaryAttributes = SecondaryAttributes.CalculateSecondaryAttributes(PrimaryAttributes);
        }
        /// <summary>
        /// Calculates characters total DPS based on weapons DPS and additions from the classes primary Attribute.
        /// </summary>
        /// <returns>Character DPS as a double</returns>
        public override double CalculateCharacterDPS()
        {
            var weapon = (Weapon)Equipment[Slots.Weapon];

            return CharacterDPS = weapon.WeaponAttributes.WeaponDPS * (1.0 + (PrimaryAttributes.Intelligence / 100.0));
        }
    }
}
