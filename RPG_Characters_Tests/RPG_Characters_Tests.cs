using RPG_Characters_v1;
using System;
using Xunit;

namespace RPG_Characters_Tests
{
    public class EquipmentTests
    {
        [Fact]
        public void EquipAxeToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Weapon testAxe = new Weapon()
            {
                Name = "Test Axe",
                Level = 1,
                EquipmentSlot = Slots.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes(15, 1.5)
            };

            // Act
            try
            {kelser.Equip(testAxe);}
            catch (Exception ex)
            {Console.WriteLine(ex);}

            string actual = kelser.Equip(testAxe);

            // Assert
            Assert.Equal(kelser.Equipment[Slots.Weapon], testAxe);
            Assert.Equal(kelser.Equip(testAxe), actual);
        }

        [Fact]
        public void EquipPlateBodyToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                EquipmentSlot = Slots.Body,
                ArmorType = ArmorTypes.Plate,
                PrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };

            // Act
            try
            { kelser.Equip(testPlateBody); }
            catch (Exception ex)
            { Console.WriteLine(ex); }

            string actual = kelser.Equip(testPlateBody);

            // Assert
            Assert.Equal(kelser.Equipment[Slots.Body], testPlateBody);
            Assert.Equal(kelser.Equip(testPlateBody), actual);
        }

        [Fact]
        public void EquipHighLevelWeaponToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Weapon testAxe = new Weapon()
            {
                Name = "Test Axe",
                Level = 2,
                EquipmentSlot = Slots.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes(15, 1.5)
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => kelser.Equip(testAxe));
        }

        [Fact]
        public void EquipInvalidWeaponToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Weapon testBow = new Weapon()
            {
                Name = "Test Bow",
                Level = 1,
                EquipmentSlot = Slots.Weapon,
                WeaponType = WeaponTypes.Bow,
                WeaponAttributes = new WeaponAttributes(20, 1.5)
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => kelser.Equip(testBow));
        }

        [Fact]
        public void EquipHighLevelArmorToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Armor testArmor = new Armor()
            {
                Name = "Test Armor",
                Level = 2,
                EquipmentSlot = Slots.Body,
                ArmorType = ArmorTypes.Plate,
                PrimaryAttributes = new PrimaryAttributes(15, 2, 5, 10)
            };


            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => kelser.Equip(testArmor));
        }

        [Fact]
        public void EquipInvalidArmorToWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            Armor testArmor = new Armor()
            {
                Name = "Test Armor",
                Level = 1,
                EquipmentSlot = Slots.Body,
                ArmorType = ArmorTypes.Cloth,
                PrimaryAttributes = new PrimaryAttributes(2, 2, 5, 12)
            };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => kelser.Equip(testArmor));
        }

        [Fact]
        public void CalculateDPSNoWeaponOnWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");

            double expected = 1.0 * (1.0 + (5 / 100.0));
            double actual = kelser.CalculateCharacterDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPSValidWeaponOnWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");
            Weapon testAxe = new Weapon()
            {
                Name = "Test Axe",
                Level = 1,
                EquipmentSlot = Slots.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes(7, 1.1)
            };

            // Act
            try
            { kelser.Equip(testAxe); }
            catch (Exception ex)
            { Console.WriteLine(ex); }

            // Act
            double expected = (7 * 1.1) * (1 + (5 / 100.0));
            double actual = kelser.CalculateCharacterDPS();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPSValidWeaponAndArmorOnWarrior()
        {
            // Arrange
            Warrior kelser = new Warrior("Kelser");
            Weapon testAxe = new Weapon()
            {
                Name = "Test Axe",
                Level = 1,
                EquipmentSlot = Slots.Weapon,
                WeaponType = WeaponTypes.Axe,
                WeaponAttributes = new WeaponAttributes(7, 1.1)
            };

            Armor testPlateBody = new Armor()
            {
                Name = "Common plate body armor",
                Level = 1,
                EquipmentSlot = Slots.Body,
                ArmorType = ArmorTypes.Plate,
                PrimaryAttributes = new PrimaryAttributes() { Vitality = 2, Strength = 5 }
            };      

            // Act
            try
            { kelser.Equip(testAxe); }
            catch (Exception ex)
            { Console.WriteLine(ex); }

            try
            { kelser.Equip(testPlateBody); }
            catch (Exception ex)
            { Console.WriteLine(ex); }

            // Act
            double expected = (7 * 1.1) * (1 + ((5+5) / 100.0));
            double actual = kelser.CalculateCharacterDPS();

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
