﻿namespace RPG_Characters_v1
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }

        public SecondaryAttributes() {}
        public SecondaryAttributes(int health, int armorRating, int elementalResistance)
        {
            Health = health;
            ArmorRating = armorRating;
            ElementalResistance = elementalResistance;
        }

        /// <summary>
        /// Calculates a new set of secondaryAttributes based on the primaryAttributes object passed in.
        /// Then returns a new SecondaryAttributes object.
        /// </summary>
        /// <param name="primaryAttributes"></param>
        /// <returns>SecondaryAttributes object.</returns>
        public SecondaryAttributes CalculateSecondaryAttributes(PrimaryAttributes primaryAttributes)
        {
            return new SecondaryAttributes
                (
                    primaryAttributes.Vitality * 10,
                    primaryAttributes.Strength + primaryAttributes.Dexterity,
                    primaryAttributes.Intelligence
                );
        }

        public override string ToString()
        {
            return $"\n\tHealth: {Health} \n\tArmor Rating: {ArmorRating} \n\tElemental Resistance: {ElementalResistance}";
        }
    }
}