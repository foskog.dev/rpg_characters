﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace RPG_Characters_v1
{
    public class PrimaryAttributes
    {
        public int Vitality { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public PrimaryAttributes(){}

        public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence)
        {
            Vitality = vitality;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        /// <summary>
        /// Overloaded + operator for increasing attributes when a character levels up or new items are equipped.
        /// Takes 2 sets of attributes. The characters current attributes and the attributes that should be added.
        /// </summary>
        /// <param name="currentAttributes"></param>
        /// <param name="gainedAttributes"></param>
        /// <returns></returns>
        public static PrimaryAttributes operator + (PrimaryAttributes currentAttributes, PrimaryAttributes gainedAttributes)
        {
            return new PrimaryAttributes()
            {
                Vitality = currentAttributes.Vitality + gainedAttributes.Vitality,
                Strength = currentAttributes.Strength + gainedAttributes.Strength,
                Dexterity = currentAttributes.Dexterity + gainedAttributes.Dexterity,
                Intelligence = currentAttributes.Intelligence + gainedAttributes.Intelligence
            };
        }
        /// <summary>
        /// Overloaded - operator for increasing attributes when a characters items are swapped out.
        /// Takes 2 sets of attributes. The characters current attributes and the attributes on the currently equipped item for subtraction.
        /// </summary>
        /// <param name="currentAttributes"></param>
        /// <param name="subtractedAttributes"></param>
        /// <returns></returns>
        public static PrimaryAttributes operator - (PrimaryAttributes currentAttributes, PrimaryAttributes subtractedAttributes)
        {
            return new PrimaryAttributes()
            {
                Vitality = currentAttributes.Vitality - subtractedAttributes.Vitality,
                Strength = currentAttributes.Strength - subtractedAttributes.Strength,
                Dexterity = currentAttributes.Dexterity -subtractedAttributes.Dexterity,
                Intelligence = currentAttributes.Intelligence - subtractedAttributes.Intelligence
            };
        }

        public override string ToString()
        {
            return $"\n\tVitality: {Vitality} \n\tStrength: {Strength} \n\tDexterity: {Dexterity} \n\tIntelligence: {Intelligence}";
        }
    }
}