﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    /// <summary>
    /// Enum that dictates all the available equipmentslots on a character.
    /// </summary>
    public enum Slots
    {
        Head,
        Body,
        Legs,
        Weapon
    }
    /// <summary>
    /// Abstract character class that every character inherits from.
    /// </summary>
    public abstract class Character
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public double CharacterDPS { get; set; }
        public PrimaryAttributes PrimaryAttributes { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; }
        public Dictionary<Slots, IItem> Equipment { get; set; }

        public Character(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slots, IItem>();
            // Init default slots.
            Equipment.Add(Slots.Head, new Armor(Slots.Head));
            Equipment.Add(Slots.Body, new Armor(Slots.Body));
            Equipment.Add(Slots.Legs, new Armor(Slots.Legs));
            Equipment.Add(Slots.Weapon, new Weapon());
        }
        /// <summary>
        /// Method for increasing a characters level and raising their attributes based on what character class they are.
        /// </summary>
        public abstract void LevelUp();
        /// <summary>
        /// Equips a weapon to a character. Checks if chosen weapon is compatible with class and that the character is at an appropriate level.
        /// Throws InvalidWeaponException in either of these cases.
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>Success message as a string.</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public abstract string Equip(Weapon weapon);
        /// <summary>
        /// Equips an armor item to a character. Checks if chosen armor is compatible with class and that the character is at an appropriate level.
        /// Throws InvalidArmor in either of these cases.
        /// </summary>
        /// <param name="armor"></param>
        /// <returns>Success message as a string.</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public abstract string Equip(Armor armor);
        /// <summary>
        /// Calculates a characters total DPS based on the classes primaryAttribute. returns the characters dps as a double.
        /// </summary>
        /// <returns>double character DPS </returns>
        public abstract double CalculateCharacterDPS();
        /// <summary>
        /// Creates a string of all the characters details with a stringBuilder and returns it.
        /// </summary>
        /// <returns>string character sheet</returns>
        public string PrintCharacterSheet()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Name - {Name}");
            sb.AppendLine($"Level - {Level}");
            sb.AppendLine($"Primary Attributes: {PrimaryAttributes}");
            sb.AppendLine($"Secondary Attributes: {SecondaryAttributes}");
            foreach (var item in Equipment)
            {
                sb.AppendLine($"{item.Key} : {item.Value.Name}");
            }
            sb.AppendLine($"{CharacterDPS}");
            return sb.ToString();
        }
    }
}