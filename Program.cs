﻿using System;

namespace RPG_Characters_v1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Warrior kelser = new Warrior("Kelser");

            Weapon bastardSword = new Weapon("Bastard Sword", 2, WeaponTypes.Sword, 20, 1.3);

            Equip(kelser, bastardSword);

            kelser.LevelUp();

            Equip(kelser, bastardSword);

            Console.WriteLine(kelser.PrintCharacterSheet());

            Armor rustyChainMail = new Armor("Rusty Chain Mail", 1, Slots.Body, ArmorTypes.Mail, 3, 1, 0, 0);

            Equip(kelser, rustyChainMail);

            Armor hackersBoots = new Armor("Hackers boots", 1, Slots.Legs, ArmorTypes.Plate, 100, 100, 100, 100);

            Equip(kelser, hackersBoots);

            Console.WriteLine(kelser.PrintCharacterSheet());

        }

        // Static equip functions that wrap in a try catch for convinience.
        static void Equip(Character character, Weapon weapon)
        {
            try
            {
                character.Equip(weapon);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }

        static void Equip(Character character, Armor armor)
        {
            try
            {
                character.Equip(armor);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
        }
    }
}
