﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    public interface IItem
    {
        public string Name { get; set; }
        public int Level{ get; set; }
        public Slots EquipmentSlot { get; set; }
    }
}
