﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    /// <summary>
    /// A set of attributes unique to weapons. Used for calculating DPS.
    /// </summary>
    public class WeaponAttributes
    {
        public int BaseDamage { get; set; }
        public double AttackSpeed { get; set; }
        public double WeaponDPS { get; set; }

        public WeaponAttributes(int baseDamage, double attackSpeed)
        {
            BaseDamage = baseDamage;
            AttackSpeed = attackSpeed;
            WeaponDPS = CalculateDamagePerSecond();
        }
        public double CalculateDamagePerSecond()
        {
            return BaseDamage * AttackSpeed;
        }
    }
}
