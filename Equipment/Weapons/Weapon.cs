﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    /// <summary>
    /// Enum that dictates all the available Weapon types.
    /// </summary>
    public enum WeaponTypes
    {
        NoWeapon,
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    public class Weapon : IItem
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public Slots EquipmentSlot { get; set; }
        public WeaponTypes WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        //public Weapon(){}

        /// <summary>
        /// Constructor for a no weapon item. Only used when instansiating an equipment dictionary on a new character. Or theoretically to unequip an item.
        /// </summary>
        public Weapon()
        {
            Name = "No Weapon";
            Level = 0;
            EquipmentSlot = Slots.Weapon;
            WeaponType = WeaponTypes.NoWeapon;
            WeaponAttributes = new WeaponAttributes(1, 1.0);
        }
        /// <summary>
        /// Constructor for any equipable weapon1 item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="weaponType"></param>
        /// <param name="baseDamage"></param>
        /// <param name="attackSpeed"></param>
        public Weapon(string name, int level, WeaponTypes weaponType, int baseDamage, double attackSpeed)
        {
            Name = name;
            Level = level;
            EquipmentSlot = Slots.Weapon;
            WeaponType = weaponType;
            WeaponAttributes = new WeaponAttributes(baseDamage, attackSpeed);
        }
    }
}
