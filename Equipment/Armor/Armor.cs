﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    /// <summary>
    /// Enum that dicatates all the available armor types
    /// </summary>
    public enum ArmorTypes
    {
        NoArmor,
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public class Armor : IItem
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public Slots EquipmentSlot { get; set; }
        public ArmorTypes ArmorType { get; set; }
        public PrimaryAttributes PrimaryAttributes { get; set; }
        
        public Armor()
        {
        }
        /// <summary>
        /// Constructor for any equipable armor item.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="equipmentSlot"></param>
        /// <param name="armortype"></param>
        /// <param name="vitality"></param>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public Armor(string name, int level, Slots equipmentSlot, ArmorTypes armortype, int vitality, int strength, int dexterity, int intelligence)
        {
            Name = name;
            Level = level;
            EquipmentSlot = equipmentSlot;
            ArmorType = armortype;
            PrimaryAttributes = new PrimaryAttributes(vitality, strength, dexterity, intelligence);
        }
        /// <summary>
        /// Constructor for a no armor item. Only used when instansiating an equipment dictionary on a new character. Or theoretically to unequip an item.
        /// </summary>
        /// <param name="equipmentSlot"></param>
        public Armor(Slots equipmentSlot)
        {
            Name = "No Armor";
            Level = 0;
            EquipmentSlot = equipmentSlot;
            ArmorType = ArmorTypes.NoArmor;
            PrimaryAttributes = new PrimaryAttributes(0, 0, 0, 0);
        }
    }
}
