﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters_v1
{
    /// <summary>
    /// Custom exception to throw when a character is unable to equip an armor.
    /// </summary>
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {

        }

        public InvalidArmorException(string message)
            : base(message)
        {

        }

        public InvalidArmorException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
